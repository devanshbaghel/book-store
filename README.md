# Book Store

This is my submission of [TOP's Shopping Cart Project](https://www.theodinproject.com/lessons/node-path-react-new-shopping-cart).

Made with [React](https://react.dev/) and [TailwindCSS](https://tailwindcss.com/). 
